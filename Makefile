pkgbuilds/%:
	git submodule add https://aur.archlinux.org/$(notdir $@).git $@
	aur depends -b $(notdir $@) | xargs -i{} $(MAKE) pkgbuilds/{}

update:
	git submodule foreach git pull
	aur depends -b $(notdir $(wildcard pkgbuilds/*)) | xargs -i{} $(MAKE) pkgbuilds/{}
	git add pkgbuilds
	git diff --submodule=diff master

.PHONY: update
