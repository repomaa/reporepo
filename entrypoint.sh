#!/bin/bash

set -euo pipefail

if [ ! -f /repo/reporepo.db.tar ]; then
    repo-add /repo/reporepo.db.tar
fi

sudo pacman -Sy

if [ $# -eq 0 ]; then
    aur graph ./*/.SRCINFO | tsort | tac > /tmp/packages
else
    printf "./%s/.SRCINFO\n" "$@" | xargs aur graph | tsort | tac > /tmp/packages
fi

export \
    LOGDEST=/log \
    PACKAGER="Reporepo CI <admin+reporepo@j.repomaa.com>" \
    MAKEFLAGS="-j$(($(nproc)+1))"

/usr/bin/aur build -a /tmp/packages -- --clean --syncdeps --noconfirm -L | tail -n100
