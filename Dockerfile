FROM archlinux/base
RUN pacman -Syu --noconfirm --needed git base-devel
ARG USER_ID
RUN mkdir -p /src/aurutils && useradd -m -u $USER_ID aur && chown aur:aur /src/aurutils
RUN echo "aur ALL=(ALL) NOPASSWD: /usr/bin/pacman *" >> /etc/sudoers

USER aur
RUN git clone https://aur.archlinux.org/aurutils.git /src/aurutils
RUN cd /src/aurutils && makepkg -i --clean --syncdeps --noconfirm

USER root
RUN echo -e "[reporepo]\nSigLevel = Optional TrustAll\nServer = file:///repo" >> /etc/pacman.conf
USER aur

ENTRYPOINT ["/entrypoint.sh"]
COPY entrypoint.sh /entrypoint.sh

VOLUME /var/cache/pacman/pkg
VOLUME /log
WORKDIR /pkgbuilds
